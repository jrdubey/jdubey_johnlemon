﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    public GameObject enemy;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public AudioSource caughtAudio;
    

    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    bool m_IsEnemyCaptured;
    float m_Timer;
    bool m_HasAudioPlayed;


    //When the player collides with the exit it makes the boolean to check if the player is at the exit true.
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            m_IsPlayerAtExit = true;
        }
    }
    //when player is caught, then it changes the boolean for the player getting caught to true.
    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }
    //when enemy is caught, then it changes the boolean for the enemy getting caught to true.
    public void EnemyCaptured()
    {
        m_IsEnemyCaptured = true;
    }

    void Update()
    {
        //if the player reaches the exit they win
        if (m_IsPlayerAtExit)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }
        //if the player is caught, they lose
        else if (m_IsPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
        //if the enemy is captured, the player wins
        if (m_IsEnemyCaptured)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }
    }

    //checks for a win or loss condition and plays the appropriate audio and displays the proper text depending.
    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if(!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }

        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;
        // restarts the level
        if (m_Timer > fadeDuration + displayImageDuration)
        {
            //restarts the level
            if (doRestart)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            }
        }
    }
}