﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIText : MonoBehaviour
{
    // Start is called before the first frame update
        public float time = 5; //Seconds to read the text

        IEnumerator Start()
        {
            yield return new WaitForSeconds(time);
            Destroy(gameObject);
        }
}
