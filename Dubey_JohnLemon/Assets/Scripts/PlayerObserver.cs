﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerObserver : MonoBehaviour
{
    public Transform enemy;
    public GameEnding GameEnding;

    public int count;

    bool m_IsEnemyCaught;

    void Start()
    {
        count = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ghost"))
        {
            count = count + 1;
            other.gameObject.SetActive(false);
        }
        if (count >= 4f)
        {
            GameEnding.EnemyCaptured();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.transform == enemy)
        {
            m_IsEnemyCaught = false;
        }
    }

    void Update()
    {
        print(count);

        //if the enemy is caught in the player's line of sight.
        if (m_IsEnemyCaught)
        {
            Vector3 direction = enemy.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;

            if (Physics.Raycast(ray, out raycastHit))
            {
                
            }



        }
        
    }
}
