﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_Audiosource;

    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    float sprintEnergy = 1f;

    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Audiosource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        print(sprintEnergy);
        //sprint function
        if (Input.GetKey(KeyCode.LeftShift))
        {
            //if player presses left shift, the character moves twice as fast.
            m_Animator.speed = 2f;
            sprintEnergy -= Time.deltaTime;
        }
        else
        {
            m_Animator.speed = 1f;
            sprintEnergy += Time.deltaTime * 0.25f;

            if (sprintEnergy > 1f)
            sprintEnergy = 1f;
              
        }
    }
    void FixedUpdate()
    {
        //sets the movement of the player
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);
        if (isWalking)
        {
            //if the player is moving it will play the audio queue
            if (!m_Audiosource.isPlaying)
            {
                m_Audiosource.Play();
            }
        }
        else
        // if they are not moving, the audio will not play
        {
            m_Audiosource.Stop();
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }
}