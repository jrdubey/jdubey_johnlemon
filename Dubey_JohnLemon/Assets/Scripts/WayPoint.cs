﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WayPoint : MonoBehaviour

{
    public NavMeshAgent NavMeshAgent;
    public Transform[] waypoints;

    // Start is called before the first frame update
    int m_CurrentWaypointIndex;

    void Start()
    {
        //locates the first waypoint for the ghost to follow
        NavMeshAgent.SetDestination(waypoints[0].position);
    }

    void Update()
    {
        //change direction and gets to next waypoint.
        if (NavMeshAgent.remainingDistance < NavMeshAgent.stoppingDistance)
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
            NavMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
        }
    }
}
