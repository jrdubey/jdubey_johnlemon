﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowSpawn : MonoBehaviour
{
    // Start is called before the first frame update
    float shadowTimer = 3f;

    public GameObject Shadow;

    bool m_IsVisible;
    bool m_IsNotVisible;

    // Update is called once per frame
    void Update()
    {
        print(shadowTimer);
        
        //if the object is visible, then the boolean is true and the timer goes down
        if (m_IsVisible == true)
        {
            shadowTimer -= Time.deltaTime;
        }
        
        //if the object is invisible, then the boolean is false and the timer goes up
        if (m_IsNotVisible == true)
        {
            shadowTimer += Time.deltaTime;
        }
        
        //if the timer goes down to 0 it does not go past 0
        if (shadowTimer < 0)
        {
            shadowTimer = 0;
        }
        
        //if the timer is set to 0, the object is invisible, not visible
        if (shadowTimer <= 0)
        {
            m_IsNotVisible = true;
            m_IsVisible = false;
            Shadow.transform.GetChild(0).gameObject.SetActive(false);
            Shadow.transform.GetChild(1).gameObject.SetActive(false);
            Shadow.transform.GetChild(2).gameObject.SetActive(false);
            Shadow.transform.GetChild(3).gameObject.SetActive(false);
        }
        //if the timer reaches 3, it doesn't go higher than 3
        if (shadowTimer >= 3)
            shadowTimer = 3;
      
        //if the timer reaches 3 it should turn visible.
        if (shadowTimer >= 3)
        {
            m_IsNotVisible = false;
            m_IsVisible = true;
            Shadow.transform.GetChild(0).gameObject.SetActive(true);
            Shadow.transform.GetChild(1).gameObject.SetActive(true);
            Shadow.transform.GetChild(2).gameObject.SetActive(true);
            Shadow.transform.GetChild(3).gameObject.SetActive(true);
        }
    }
}
